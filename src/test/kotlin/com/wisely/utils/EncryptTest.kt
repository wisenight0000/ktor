package com.wisely.utils

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import kotlin.test.assertEquals

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class EncryptTest {
    @Test
    fun test00MD5() {
        var result = Encrypt.md5("s0mRIdlKvI", "1234")
        assertEquals("2d7c308c232a9cf30757dedcce054d61", result)

        result = Encrypt.md5("1209e24b-0357-4120-b97f-624cbbea3281", "567zzz1@#")
        assertEquals("0bea7643434803b4442f90d9297d09e2", result)
    }
}