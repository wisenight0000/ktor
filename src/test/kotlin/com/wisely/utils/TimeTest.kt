package com.wisely.utils

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import kotlin.test.assertEquals
import kotlin.test.assertNull

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class TimeTest {

    @Test
    fun test00ConvertFromDateString() {
        var date = Time.convertFromDateString("2022-03-31")
        assertEquals("2022-03-31T00:00:00.000+0000", Time.convertDateToString(date!!))

        date = Time.convertFromDateString("2022-03-32")
        assertEquals("2022-04-01T00:00:00.000+0000", Time.convertDateToString(date!!))

        date = Time.convertFromDateString("2022-03-31")
        assertEquals("2022-03-31", Time.convertDateToSimpleString(date!!))

        date = Time.convertFromDateString("2022-03-32")
        assertEquals("2022-04-01", Time.convertDateToSimpleString(date!!))

        date = Time.convertFromDateString("2022/03/32:00:00:00+0800")
        assertNull(date)
    }
}