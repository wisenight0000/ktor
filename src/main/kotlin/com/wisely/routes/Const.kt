package com.wisely.routes

class Const {
    companion object {
        const val ID = "id"
        const val USER_ID = "userId"
        const val STATUS = "status"
        const val PRIORITY = "priority"
        const val SORT = "sort"
        const val CURSOR = "cursor"
        const val LIMIT = "limit"
    }
}