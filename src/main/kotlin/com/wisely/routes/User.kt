package com.wisely.routes

import com.wisely.models.User
import com.wisely.routes.error.TSMCError
import com.wisely.routes.request.LoginRequest
import com.wisely.routes.request.RegisterRequest
import com.wisely.routes.response.ErrorResponse
import com.wisely.routes.response.LoginResponse
import com.wisely.routes.response.SuccessResponse
import com.wisely.routes.response.TokenResponse
import com.wisely.routes.utils.JWT.Companion.createToken
import com.wisely.utils.CID
import com.wisely.utils.Encrypt
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.util.*

fun Route.userRouting() {
    route("") {
        registerRoute()
        loginRoute()
        authenticate("jwt-auth") {
            tokenRoute()
        }
    }
}

fun Route.registerRoute() {
    post("/register") {
        val cid = CID.gen()
        val registerRequest = call.receive<RegisterRequest>()
        if (registerRequest.email.isBlank() || registerRequest.password.isBlank() || registerRequest.password.length < 6) {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse(cid, TSMCError.INVALID_INPUT, "invalid input"))
            return@post
        }
        val hash = UUID.randomUUID().toString()
        val md5Password = try {
            Encrypt.md5(hash, registerRequest.password)
        } catch (e: Exception) {
            application.log.error("md5 error: $e")
            null
        }
        if (md5Password.isNullOrBlank()) {
            call.respond(
                HttpStatusCode.InternalServerError,
                ErrorResponse(cid, TSMCError.REGISTER_ENCRYPT_FAILED, "encrypt error")
            )
            return@post
        }
        val id = User.insertUser(registerRequest.email, md5Password, hash)
        if (id.isBlank()) {
            call.respond(
                HttpStatusCode.BadRequest, ErrorResponse(cid, TSMCError.REGISTER_USER_INSERT_FAILED, "register error")
            )
            return@post
        }
        call.respond(HttpStatusCode.OK, SuccessResponse(cid, ""))
    }
}

fun Route.loginRoute() {
    post("/login") {
        val cid = CID.gen()
        val loginRequest = call.receive<LoginRequest>()
        if (loginRequest.email.isBlank() || loginRequest.password.isBlank()) {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse(cid, TSMCError.INVALID_INPUT, "invalid input"))
            return@post
        }
        val user = User.getUserByEmail(loginRequest.email) ?: run {
            call.respond(HttpStatusCode.NotFound, ErrorResponse(cid, TSMCError.LOGIN_USER_NOT_FOUND, "user not found"))
            return@post
        }
        val checkPassword = Encrypt.md5(user.hash, loginRequest.password)
        if (user.password != checkPassword) {
            call.respond(
                HttpStatusCode.BadRequest, ErrorResponse(cid, TSMCError.LOGIN_INVALID_PASSWORD, "invalid password")
            )
            return@post
        }
        val token = createToken(application, user.id)
        val loginResponse = LoginResponse(
            userId = user.id,
            email = user.email,
            token.token, token.expired
        )
        call.respond(HttpStatusCode.OK, SuccessResponse(cid, loginResponse))
    }
}

fun Route.tokenRoute() {
    post("/token") {
        val cid = CID.gen()
        val jwtResult = com.wisely.routes.utils.JWT.checkJWTToken(call)
        if (!jwtResult.isValid) {
            call.respond(HttpStatusCode.Unauthorized, ErrorResponse(cid, TSMCError.INVALID_TOKEN))
            return@post
        }
        val userId = jwtResult.userId
        val token = createToken(application, userId)
        val loginResponse = TokenResponse(
            token.token, token.expired
        )
        call.respond(HttpStatusCode.OK, SuccessResponse(cid, loginResponse))
    }
}