package com.wisely.routes.response

class SuccessResponse<T>(val cid: String, val data: T) {

}

data class ErrorResponse(val cid: String, val errorCode: Int, val errorMessage: String = "")

