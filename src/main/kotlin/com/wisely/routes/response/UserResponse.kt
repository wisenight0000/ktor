package com.wisely.routes.response

data class LoginResponse(
    val userId: String,
    val email: String,
    val token: String,
    val expired: Long,
)

data class TokenResponse(
    val token: String,
    val expired: Long,
)