package com.wisely.utils

import java.security.MessageDigest

class Encrypt {
    companion object {
        @Throws(Exception::class)
        fun md5(salt: String, plainText: String): String {
            val md = MessageDigest.getInstance("MD5")
            md.update(salt.toByteArray())
            md.update(plainText.toByteArray())
            val byteData = md.digest()
            val sb = StringBuffer()
            for (b in byteData) {
                sb.append(String.format("%02x", b))
            }
            return sb.toString()
        }
    }
}