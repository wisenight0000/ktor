package com.wisely

import com.wisely.modules.*
import io.ktor.server.application.*
import io.ktor.server.netty.*

fun main(args: Array<String>): Unit = EngineMain.main(args)

fun Application.main() {
    configMongoDB()
    configureSerialization()
    configureSecurity()
    configureHTTP()
    configureRouting()
}