let taskMap = new Map();

window.onload = async () => {
    $("#registerButton").click(register);
    $("#loginButton").click(login);
    $("#showCreateTaskButton").click(showCreateTask);
    $("#createTaskButton").click(createTask);
    let createTaskDueDateInput = $("#createTaskDueDateInput");
    createTaskDueDateInput.datepicker();
    createTaskDueDateInput.datepicker("option", "dateFormat", "yy-mm-dd");
    $("#modifyTaskButton").click(modifyTask);
    let modifyTaskDueDateInput = $("#modifyTaskDueDateInput");
    modifyTaskDueDateInput.datepicker();
    modifyTaskDueDateInput.datepicker("option", "dateFormat", "yy-mm-dd");
    $("#filterButton").click(getTaskList);
    $("#loadMoreButton").click(loadMoreTaskList);
    $("#createTaskContainer").dialog({
        autoOpen: false,
        width: 512,
    });
    $("#modifyTaskContainer").dialog({
        autoOpen: false,
        width: 512,
    });
    let tokenResult = await tokenAPI();
    let code = tokenResult["errorCode"];
    if (!code) {
        let data = tokenResult["data"];
        let token = data["token"];
        sessionStorage.setItem("token", token);
        $("#accountContainer").hide();
        $("#taskContainer").show();
        await getTaskList();
    } else {
        $("#accountContainer").show();
        $("#taskContainer").hide();
    }
};

register = async () => {
    let errorStatus = $("#accountErrorStatus");
    let successStatus = $("#accountSuccessStatus");
    errorStatus.text("");
    successStatus.text("");
    let email = $("#emailInput").val();
    let password = $("#passwordInput").val();
    let registerResult = await registerAPI(email, password);
    if (!registerResult) {
        errorStatus.text(`register failed`);
        return
    }
    let code = registerResult["errorCode"];
    if (!code) {
        successStatus.text((`register ok`));
    } else {
        errorStatus.text(`register failed, error: ${code}`);
    }
}

login = async () => {
    let errorStatus = $("#accountErrorStatus");
    let successStatus = $("#accountSuccessStatus");
    errorStatus.text("");
    successStatus.text("");
    let email = $("#emailInput").val();
    let password = $("#passwordInput").val();
    let loginResult = await loginAPI(email, password);
    if (!loginResult) {
        errorStatus.text(`login failed`);
        return
    }
    let code = loginResult["errorCode"];
    if (!code) {
        let data = loginResult["data"];
        let token = data["token"];
        successStatus.text((`login ok`));
        sessionStorage.setItem("token", token);
        $("#accountContainer").hide();
        $("#taskContainer").show();
        await getTaskList()
    } else {
        errorStatus.text(`login failed, error: ${code}`);
    }
}

getTaskList = async () => {
    let status = $("#statusFilterSelector").val();
    let priority = $("#priorityFilterSelector").val();
    let sort = $("#sortSelector").val();
    setLoadMoreCursor("");
    let taskListResult = await getTaskListAPI(status, priority, sort);
    if (!taskListResult) {
        return
    }
    taskMap.clear();
    let taskTableBody = $("#taskTableBody");
    taskTableBody.empty();
    let code = taskListResult["errorCode"];
    if (!code) {
        let data = taskListResult["data"];
        let list = data["list"];
        let nextCursor = data["nextCursor"];
        for (let i = 0; i < list.length; i++) {
            let task = list[i];
            taskMap[task["id"]] = task;
            taskTableBody.append(getItemHtml(task));
        }
        setLoadMoreCursor(nextCursor);
    } else {
        console.error(`get task failed: ${code}`);
    }
}

loadMoreTaskList = async () => {
    let status = $("#statusFilterSelector").val();
    let priority = $("#priorityFilterSelector").val();
    let sort = $("#sortSelector").val();
    let cursor = $("#loadMoreSpan").text();
    setLoadMoreCursor("");
    let taskListResult = await getTaskListAPI(status, priority, sort, cursor);
    if (!taskListResult) {
        return
    }
    let taskTableBody = $("#taskTableBody");
    let code = taskListResult["errorCode"];
    if (!code) {
        let data = taskListResult["data"];
        let list = data["list"];
        let nextCursor = data["nextCursor"];
        for (let i = 0; i < list.length; i++) {
            let task = list[i];
            taskMap[task["id"]] = task;
            taskTableBody.append(getItemHtml(task));
        }
        setLoadMoreCursor(nextCursor);
    } else {
        console.error(`load more task failed: ${code}`);
    }
}

function showCreateTask() {
    let createTaskDialog = $('#createTaskContainer');
    createTaskDialog.dialog('close');
    let errorStatus = $("#createTaskErrorStatus");
    let successStatus = $("#createTaskSuccessStatus");
    errorStatus.text("");
    successStatus.text("");
    createTaskDialog.dialog('open');
}

function showModifyTask(id) {
    let task = taskMap[id];
    if (!task) {
        return
    }
    let modifyTaskDialog = $('#modifyTaskContainer');
    modifyTaskDialog.dialog('close');
    let errorStatus = $("#modifyTaskErrorStatus");
    let successStatus = $("#modifyTaskSuccessStatus");
    errorStatus.text("");
    successStatus.text("");
    $("#modifyTaskId").text(id);
    $("#modifyTaskStatusInput").val(task["status"]);
    $("#modifyTaskTitleInput").val(task["title"]);
    $("#modifyTaskDueDateInput").val(task["dueDate"]);
    $("#modifyTaskPrioritySelector").val(task["priority"]);
    modifyTaskDialog.dialog('open');
}

createTask = async () => {
    let errorStatus = $("#createTaskErrorStatus");
    let title = $("#createTaskTitleInput").val();
    let dueDate = $("#createTaskDueDateInput").val();
    let priority = $("#createTaskPrioritySelector").val();
    let createTaskResult = await createTaskAPI(title, priority, dueDate);
    if (!createTaskResult) {
        errorStatus.text(`create task failed`);
        return
    }
    let code = createTaskResult["errorCode"];
    if (!code) {
        await getTaskList();
        let createTaskDialog = $('#createTaskContainer');
        createTaskDialog.dialog('close');
    } else {
        errorStatus.text(`create task failed, error: ${code}`);
    }
}

modifyTask = async () => {
    let errorStatus = $("#modifyTaskErrorStatus");
    let id = $("#modifyTaskId").text();
    let title = $("#modifyTaskTitleInput").val();
    let dueDate = $("#modifyTaskDueDateInput").val();
    let priority = $("#modifyTaskPrioritySelector").val();
    let modifyTaskResult = await modifyTaskAPI(id, null, title, priority, dueDate);
    if (!modifyTaskResult) {
        errorStatus.text(`modify failed`);
        return
    }
    let code = modifyTaskResult["errorCode"];
    if (!code) {
        await getTaskList();
        let modifyTaskDialog = $('#modifyTaskContainer');
        modifyTaskDialog.dialog('close');
    } else {
        errorStatus.text(`modify task failed, error: ${code}`);
    }
}

changeTaskStatus = async (id, status) => {
    let modifyTaskResult = await modifyTaskAPI(id, status);
    if (!modifyTaskResult) {
        console.error(`change status failed`);
        return
    }
    let code = modifyTaskResult["errorCode"];
    if (!code) {
        await getTaskList();
    } else {
        console.error(`change status failed, error: ${code}`);
    }
}

function setLoadMoreCursor(cursor) {
    let loadMoreContainer = $("#loadMoreContainer");
    let loadMoreSpan = $("#loadMoreSpan");
    if (!cursor) {
        loadMoreContainer.hide();
        loadMoreSpan.text("");
    } else {
        loadMoreContainer.show();
        loadMoreSpan.text(`${cursor}`);
    }
}

function getItemHtml(task) {
    let created = new Date(task["createdTime"]);
    let dueDate = new Date(task["dueDate"]);
    let id = task["id"];
    let status = task["status"];
    let processingDisplay = "none";
    let finishedDisplay = "none";
    let cancelDisplay = "none";
    let initDisplay = "none";
    if (status === 'Init') {
        processingDisplay = "inline";
        cancelDisplay = "inline";
    } else if (status === "Processing") {
        finishedDisplay = "inline";
        cancelDisplay = "inline";
    } else if (status === "Cancel") {
        initDisplay = "inline";
    }
    return `
<tr>
  <td>${id}</td>
  <td>${status}</td>
  <td>${task["priority"]}</td>
  <td>${task["title"]}</td>
  <td>${created.toLocaleString()}</td>
  <td>${dueDate.toLocaleString()}</td>
  <td>
    <button onclick="showModifyTask('${id}')">Modify</button>
    <button onclick="changeTaskStatus('${id}', 'Processing')" style="display:${processingDisplay}">Processing</button>
    <button onclick="changeTaskStatus('${id}', 'Finished')" style="display:${finishedDisplay}">Finished</button>
    <button onclick="changeTaskStatus('${id}', 'Cancel')" style="display:${cancelDisplay}">Cancel</button>
    <button onclick="changeTaskStatus('${id}', 'Init')" style="display:${initDisplay}">Init</button>
  </td>
</tr>
`;
}